# Wardrobify

Team:

* Shashwath - Hats
* Marc - Shoes

## Design

## Shoes microservice

1) Using Shoes from the wardrobe API I was then able to build out a BinVO and Shoes model in order with the help of poller.py so that my information is able to be accessed and stored.

2) fterwards, I then through the backend were able to build the shoes list, shoes creation, shoes, deletion, as well as showing the details of the shoes.

3) In order for the information to be linked together I utilized the urls so that the user is able to access the information upon navigating through our Nav.js as well as our hyperlinks within our code. Using http://localhost:8080 for shoes urls so the backend would be able to work.

4) Afterwards, I then proceeded to React where I built out the functionality of the ShoesList Page, ShoesForm Page, ShoesDetail Page. Within the ShoesList Page, we have the ability to delete any of the shoes that we have created.

5) Then within our App.js, we then added the routing using http://localhost:3000 so that the information is able to be routed and accessed.

## Hats microservice

1. I built out a Hat and LocationVO model that pulled information from the wardrobe API using poller.py

2. From there, I built out backend functionality for listing hats, showing hat details, creating a hat, deleting a hat, and updating a hat using views.

3. I also built out urls using the base http://localhost:8090 for hats to show the different backend functionality from views.

4. After this, I moved to React where I built out HatsList page, a HatsDetail page, and a HatsForm page for viewing a list of hats, viewing the details of the hat, and deleting the hat

5. From here, I added and created url links for these pages using the base http://localhost:3000, and I used a nested Route to add a /hats to the base.


## Fun Additional Functionality

1. We built out a ClosetList page that lists out the locations and bins that are created in our database.

2. From here, we created a HatClosetDetail page and a ShoesClosetDetail page that has a list of hats or shoes that are specified to the specific location or bin that they were assigned to. These pages also link to each of the hats' and shoes' detail pages as well.

## FUN STUFF
1) We added a ClosetList page for the Locations and Bins from the Wardrobe database.

2) Afterwards we created a HatClosetDetail page ShoesClosetDetail page that contais the list of hats / shoes in the specific location or bin that they were assigned to. The pages are linked to either hats' or shoes' detail pages so when the user clicks on the hats/shoes within the specified closet detail page it then goes into the hats / shoes detail pages.

3) In order to add information within the ClosetList we created a HatClosetForm as well as a ShoeClosetForm page to be able to create the location and bin from the wardrobe database.

![README](./ghi/images/WardrobifyClosetFunctionality.png)

3. We also created a HatClosetForm page and ShoeClosetForm page to be able create a location or bin in our wardrobe microservice as well.
