import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

function HatsList() {
    const [hats, setHats] = useState([]);

    const fetchData = async () => {

        const url = 'http://localhost:8090/api/hats/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setHats(data.hats)
            }
          }

    useEffect(() => {
        fetchData();
    }, []);


    const handleDelete = async (event) => {

        const value=event.target.value;
        const hatUrl = `http://localhost:8090/api/hats/${value}`;
        const fetchConfig = {
            method: "Delete",
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const hatResponse = await fetch(hatUrl, fetchConfig);
        if (hatResponse.ok) {
            const deletedHat = await hatResponse.json();
            console.log(deletedHat);

            setHats(hats.filter(hat => String(hat.id) !== value))

        }

        }


    return(
        <div>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Hat Name</th>
                    <th>Closet Name</th>
                </tr>
            </thead>
            <tbody>
                {hats.map(hat => {
                    return (
                        <tr key={hat.href} value={hat.href}>
                            <td><Link to={`/hats/${hat.id}/`}>{ hat.style_name }</Link></td>
                            <td>{ hat.location }</td>
                            <td><button onClick={handleDelete} value={hat.id} className="btn btn-danger">Delete</button></td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
        </div>
);
};

export default HatsList;
