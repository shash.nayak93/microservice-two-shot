import React, { useState } from 'react';

function HatsClosetForm() {
    const [closetName, setClosetName] = useState("");
    const [sectionNumber, setSectionNumber] = useState("");
    const [shelfNumber, setShelfNumber] = useState("");

    const handleClosetName = (e) => {
      const value = e.target.value;
      setClosetName(value)
      }

    const handleSectionNumberChange = (e) => {
      const value = e.target.value;
      setSectionNumber(value)
      }

    const handleShelfNumberChange = (e) => {
      const value = e.target.value;
        setShelfNumber(value)
      }

    const handleSubmit = async (event) => {
      event.preventDefault();

    const data = {};

    data.closet_name = closetName;
    data.section_number = sectionNumber;
    data.shelf_number = shelfNumber;

    const shoeClosetFormUrl = 'http://localhost:8100/api/locations/';
    const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
    };

    const shoeResponse = await fetch(shoeClosetFormUrl, fetchConfig);
    if (shoeResponse.ok) {
        const newShoe = await shoeResponse.json();
        console.log(newShoe);

        setClosetName('');
        setSectionNumber('');
        setShelfNumber('');

    }
  }

    return(
        <>
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new hat closet</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange = {handleClosetName} value={closetName} placeholder="closetName" required type="text" id="closet_name" name="closet_name" className="form-control" />
                <label htmlFor="closet_name">Closet Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleSectionNumberChange} value={sectionNumber} placeholder="sectionNumber" required type="number" id="section_number" name="section_number" className="form-control" />
                <label htmlFor="sectionNumber">Section Number</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleShelfNumberChange} value={shelfNumber} placeholder="shelfNumber" required type="number" id="shelf_number" name="shelf_number" className="form-control" />
                <label htmlFor="shelfNumber">Shelf Number</label>
              </div>
              {/* <div className="mb-3">
                <select onChange = {handleBinChange} value={bin} required id="bins" name="bins" className="form-select">
                  <option value="">Choose a bin</option>
                      {bins.map(bin => {
                        return (
                          <option key={bin.href} value={bin.href}>
                              {bin.closet_name}
                          </option>
                        );
                      })}
                </select>
              </div> */}
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
      </>
    );
}

export default HatsClosetForm;
