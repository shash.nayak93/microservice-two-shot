import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatsList from './HatsList';
import HatForm from './HatForm';
import HatDetail from './HatDetail';
import ShoesList from './ShoesList';
import ShoesForm from './ShoesForm';
import ShoesDetail from './ShoesDetail';
import ClosetList from './ClosetList';
import HatClosetDetail from './HatClosetDetail';
import ShoesClosetDetail from './ShoesClosetDetail';
import ShoesClosetForm from './ShoesClosetForm';
import HatsClosetForm from './HatClosetForm';

function App() {

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="hats">
            <Route index element={<HatsList />} />
            <Route path="new" element={<HatForm />} />
            <Route path=":id" element={<HatDetail />} />
          </Route>
          <Route path="shoes">
            <Route index element={<ShoesList />} />
            <Route path="new" element={<ShoesForm />} />
            <Route path=":id" element={<ShoesDetail />} />
          </Route>
          <Route path="closets">
            <Route index element={<ClosetList />} />
            <Route path="shoes/new" element={<ShoesClosetForm />} />
            <Route path="hats/new" element={<HatsClosetForm />} />
            <Route path="hats/:id" element={<HatClosetDetail />} />
            <Route path="shoes/:id" element={<ShoesClosetDetail />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
