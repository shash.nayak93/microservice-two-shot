import React, { useEffect, useState } from 'react';
import { useParams, Link } from 'react-router-dom';

function HatClosetDetail() {
    const { id } = useParams();
    const [location, setLocation] = useState([]);
    const [hats,setHats] = useState([]);


    const fetchLocationData = async () => {

        const url = `http://localhost:8100/api/locations/${id}`;
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setLocation(data);
            }
          }

    useEffect(() => {
        fetchLocationData();
    }, []);

    const fetchHatData = async () => {

        const url = 'http://localhost:8090/api/hats/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            console.log(data);
            setHats(data.hats)
            }
          }

    useEffect(() => {
        fetchHatData();
    }, []);


    return(
        <div>
            <h1>{location.closet_name}</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Hats</th>
                    </tr>
                </thead>
                <tbody>
                    {hats.map(hat => {
                        if(hat.location===location.closet_name)
                        return (
                            <tr key={hat.href} value={hat.href}>
                                <td><Link to={`/hats/${hat.id}/`}>{ hat.style_name }</Link></td>
                                {/* <td><button onClick={handleLocationDelete} value={location.id} className="btn btn-danger">Delete</button></td> */}
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
        )
}

export default HatClosetDetail;