import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

function ClosetList() {
    const [locations, setLocations] = useState([]);
    const [bins, setBins] = useState([]);

    const fetchLocationData = async () => {

        const url = 'http://localhost:8100/api/locations/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations)
            }
          }

    useEffect(() => {
        fetchLocationData();
    }, []);


    const handleLocationDelete = async (event) => {

        const value=event.target.value;
        const locationUrl = `http://localhost:8100/api/locations/${value}`;
        const fetchConfig = {
            method: "Delete",
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const locationResponse = await fetch(locationUrl, fetchConfig);
        if (locationResponse.ok) {
            const deletedLocation = await locationResponse.json();
            console.log(deletedLocation);

            setLocations(locations.filter(location => String(location.id) !== value))

        }

        }


    const fetchBinData = async () => {

        const url = 'http://localhost:8100/api/bins/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setBins(data.bins)
            }
            }

    useEffect(() => {
        fetchBinData();
    }, []);


    const handleBinDelete = async (event) => {

        const value=event.target.value;
        const binUrl = `http://localhost:8100/api/bins/${value}`;
        const fetchConfig = {
            method: "Delete",
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const binResponse = await fetch(binUrl, fetchConfig);
        if (binResponse.ok) {
            const deletedBin = await binResponse.json();
            console.log(deletedBin);

            setBins(bins.filter(bin => String(bin.id) !== value))

        }

        }


    return(
        <div>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Hat Closet</th>
                    <th># of Hats</th>
                </tr>
            </thead>
            <tbody>
                {locations.map(location => {
                    return (
                        <tr key={location.href} value={location.href}>
                            <td><Link to={`/closets/hats/${location.id}/`}>{ location.closet_name }</Link></td>
                            <td>{ location.length }</td>
                            <td><button onClick={handleLocationDelete} value={location.id} className="btn btn-danger">Delete</button></td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Shoe Closet</th>
                    <th># of Shoes</th>
                </tr>
            </thead>
            <tbody>
                {bins.map(bin => {
                    return (
                        <tr key={bin.href} value={bin.href}>
                            <td><Link to={`/closets/shoes/${bin.id}/`}>{ bin.closet_name }</Link></td>
                            <td>{ bin.length }</td>
                            <td><button onClick={handleBinDelete} value={bin.id} className="btn btn-danger">Delete</button></td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
        </div>
);
};

export default ClosetList;
