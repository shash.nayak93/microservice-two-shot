import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';

function HatDetail() {
  const [hat, setHat] = useState({});
  const { id } = useParams();
  const fetchData = async () => {

    const url = `http://localhost:8090/api/hats/${id}`;
    const fetchConfig = {
      method: "get",
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      const data = await response.json();
      setHat(data)
    }
  }

  useEffect(() => {
    fetchData();
  }, [])

  if(!hat) {
    return <p>loading</p>
  }
  return (
    <div key={hat.href} className="card mb-3 shadow w-25 p-3 h-25 d-inline-block">
      <img src={hat.picture_url} className="card-img-top" />
      <div className="card-body">
        <h5 className="card-title">{hat.style_name}</h5>
        <h6 className="card-subtitle mb-2 text-muted">
            {hat.location?.closet_name}
        </h6>
        <p className="card-text">
          Color: {hat.color}
        </p>
        <p className="card-text">
          Fabric: {hat.fabric}
        </p>
      </div>
    </div>
  );
  }

export default HatDetail;