import React, { useEffect, useState } from "react";
import { Link } from 'react-router-dom';

function ShoesList() {

    const [ shoes, setShoes ] = useState([])

    const fetchData = async () => {

        const url = "http://localhost:8080/api/shoes/";
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setShoes(data.shoes)
            }
        }

    useEffect(() => {
        fetchData();
    }, []);



        const handleDelete = async (event) => {

        const value=event.target.value;
        const shoeUrl = `http://localhost:8080/api/shoes/${value}`;
        const fetchConfig = {
            method: "Delete",
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const shoeResponse = await fetch(shoeUrl, fetchConfig);
        if (shoeResponse.ok) {
            const deletedShoe = await shoeResponse.json();
            console.log(deletedShoe);

            setShoes(shoes.filter(shoe => String(shoe.id) !== value))

        }

        }


    return(
        <div>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Shoe Name</th>
                    <th>Closet Name</th>
                </tr>
            </thead>
            <tbody>
                {shoes.map(shoe => {
                    return (
                        <tr key={shoe.href} value={shoe.href}>
                            <td><Link to={`/shoes/${shoe.id}/`}>{ shoe.modelName }</Link></td>
                            <td>{ shoe.bins }</td>
                            <td><button onClick={handleDelete} value={shoe.id} className="btn btn-danger">Delete</button></td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
        </div>
);
};

export default ShoesList;
