import React, { useState } from 'react';

function ShoesClosetForm() {
    const [closetName, setClosetName] = useState("");
    const [binNumber, setBinNumber] = useState("");
    const [binSize, setBinSize] = useState("");

    const handleClosetName = (e) => {
      const value = e.target.value;
      setClosetName(value)
      }

    const handleBinNumberChange = (e) => {
      const value = e.target.value;
      setBinNumber(value)
      }

    const handleBinSizeChange = (e) => {
      const value = e.target.value;
        setBinSize(value)
      }

    const handleSubmit = async (event) => {
      event.preventDefault();

    const data = {};

    data.closet_name = closetName;
    data.bin_number = binNumber;
    data.bin_size = binSize;

    const shoeClosetFormUrl = 'http://localhost:8100/api/bins/';
    const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
    };

    const shoeResponse = await fetch(shoeClosetFormUrl, fetchConfig);
    if (shoeResponse.ok) {
        const newShoe = await shoeResponse.json();
        console.log(newShoe);

        setClosetName('');
        setBinNumber('');
        setBinSize('');

    }
  }

    return(
        <>
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new shoe closet</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange = {handleClosetName} value={closetName} placeholder="closetName" required type="text" id="closet_name" name="closet_name" className="form-control" />
                <label htmlFor="closet_name">Closet Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleBinNumberChange} value={binNumber} placeholder="binNumber" required type="number" id="bin_number" name="bin_number" className="form-control" />
                <label htmlFor="binNumber">Bin Number</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleBinSizeChange} value={binSize} placeholder="binSize" required type="number" id="bin_size" name="bin_size" className="form-control" />
                <label htmlFor="binSize">Bin Size</label>
              </div>
              {/* <div className="mb-3">
                <select onChange = {handleBinChange} value={bin} required id="bins" name="bins" className="form-select">
                  <option value="">Choose a bin</option>
                      {bins.map(bin => {
                        return (
                          <option key={bin.href} value={bin.href}>
                              {bin.closet_name}
                          </option>
                        );
                      })}
                </select>
              </div> */}
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
      </>
    );
}

export default ShoesClosetForm;
