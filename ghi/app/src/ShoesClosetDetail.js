import React, { useEffect, useState } from 'react';
import { useParams, Link } from 'react-router-dom';

function ShoesClosetDetail() {
    const { id } = useParams();
    const [bin, setBin] = useState([]);
    const [shoes,setShoes] = useState([]);


    const fetchBinData = async () => {

        const url = `http://localhost:8100/api/bins/${id}`;
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            console.log(data);
            setBin(data);
            }
          }

    useEffect(() => {
        fetchBinData();
    }, []);

    const fetchShoeData = async () => {

        const url = 'http://localhost:8080/api/shoes/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            console.log(data);
            setShoes(data.shoes)
            }
          }

    useEffect(() => {
        fetchShoeData();
    }, []);


    return(
        <div>
            <h1>{bin.closet_name}</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Shoes</th>
                    </tr>
                </thead>
                <tbody>
                    {shoes.map(shoe => {
                        if(shoe.bins===bin.closet_name)
                        return (
                            <tr key={shoe.href} value={shoe.href}>
                                <td><Link to={`/shoes/${shoe.id}/`}>{ shoe.modelName }</Link></td>
                                {/* <td><button onClick={handleLocationDelete} value={location.id} className="btn btn-danger">Delete</button></td> */}
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
        )
}

export default ShoesClosetDetail;