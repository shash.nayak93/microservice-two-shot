import React, { useEffect, useState } from "react";
import {useParams} from "react-router-dom";

function ShoesDetail() {

const [shoes, setShoes] = useState ([]);
const {id} = useParams();
const fetchData = async () =>{

    const url = `http://localhost:8080/api/shoes/${id}`;
    const fetchConfig = {
        method: "get",
        headers: {
            "Content-Type": "application/json",
        },
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
        const data = await response.json();
        setShoes(data)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div key={shoes.href} className = "card mb-3 shadow d-inline-block">
            <img src={shoes.picture_url} className="card-img-top" />
            <div className="card-body">
                <h5 className="card-title">{shoes.modelName}</h5>
                <h6 className="card-subtitle mb-2 text-muted">
                {shoes.bins?.closet_name}
                </h6>
                <p className="card-text">
                    {shoes.color}
                </p>
                <p className="card-text">
                    {shoes.modelName}
                </p>
            </div>
        </div>
    )

}

export default ShoesDetail;
