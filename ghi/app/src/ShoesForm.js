import React, { useEffect, useState } from 'react';

function ShoeForm() {
    const [bins, setBins] = useState([]);
    const [bin, setBin] = useState([]);
    const [manufacturer, setManufacturer] = useState("");
    const [modelName, setModelName] = useState("");
    const [color, setColor] = useState("");
    const [pictureUrl, setPictureUrl] = useState("");

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setBins(data.bins)
            }
          }

    useEffect(() => {
        fetchData();
    }, []);

    const handleBinChange = (e) => {
      const value = e.target.value;
        setBin(value)
      }

    const handleManufacturerChange = (e) => {
      const value = e.target.value;
      setManufacturer(value)
      }

    const handleModelNameChange = (e) => {
      const value = e.target.value;
      setModelName(value)
      }

    const handleColorChange = (e) => {
      const value = e.target.value;
      setColor(value)
      }

    const handlePictureUrlChange = (e) => {
      const value = e.target.value;
      setPictureUrl(value)
      }

    const handleSubmit = async (event) => {
      event.preventDefault();

    const data = {};

    data.manufacturer = manufacturer;
    data.modelName = modelName;
    data.color = color;
    data.picture_url = pictureUrl;
    data.bins=bin;

    const shoeUrl = 'http://localhost:8080/api/shoes/';
    const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
    };

    const shoeResponse = await fetch(shoeUrl, fetchConfig);
    if (shoeResponse.ok) {
        const newShoe = await shoeResponse.json();
        console.log(newShoe);

        setManufacturer('');
        setModelName('');
        setColor('');
        setPictureUrl('');
        setBin('');

    }
  }

    return(
        <>
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new shoe</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange = {handleManufacturerChange} value={manufacturer} placeholder="Manufacturer" required type="text" id="manufacturer" name="manufacturer" className="form-control" />
                <label htmlFor="manufacturer">Manufacturer</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleModelNameChange} value={modelName} placeholder="Model Name" required type="text" id="modelName" name="modelName" className="form-control" />
                <label htmlFor="modelName">Model Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleColorChange} value={color} placeholder="Color" required type="text" id="color" name="color" className="form-control" />
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlePictureUrlChange} value={pictureUrl} placeholder="Picture URL" required type="text" id="picture_url" name="picture_url" className="form-control" />
                <label htmlFor="pictureUrl">Picture URL</label>
              </div>
              <div className="mb-3">
                <select onChange = {handleBinChange} value={bin} required id="bins" name="bins" className="form-select">
                  <option value="">Choose a bin</option>
                      {bins.map(bin => {
                        return (
                          <option key={bin.href} value={bin.href}>
                              {bin.closet_name}
                          </option>
                        );
                      })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
      </>
    );
}

export default ShoeForm;
