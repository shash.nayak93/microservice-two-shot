import React, { useEffect, useState } from 'react';

function HatForm() {
    const [locations,setLocations] = useState([]);
    const [fabric, setFabric] = useState('');
    const [styleName, setStyleName] = useState('');
    const [color, setColor] = useState('');
    const [pictureUrl, setPictureUrl] = useState('');
    const [location, setLocation] = useState('');

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations)
            }
          }

    useEffect(() => {
        fetchData();
    }, []);

    const handleFabricChange = (event) => {
        const value = event.target.value;
        setFabric(value);
    }

    const handleStyleNameChange = (event) => {
        const value = event.target.value;
        setStyleName(value);
    }

    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const handlePictureUrlChange = (event) => {
        const value = event.target.value;
        setPictureUrl(value);
    }

    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        // create an empty JSON object
        const data = {};

        data.fabric = fabric;
        data.style_name = styleName;
        data.color = color;
        data.picture_url = pictureUrl;
        data.location=location;

        const hatUrl = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const hatResponse = await fetch(hatUrl, fetchConfig);
        if (hatResponse.ok) {
            const newHat = await hatResponse.json();
            console.log(newHat);

            setFabric('');
            setStyleName('');
            setColor('');
            setPictureUrl('');
            setLocation('');

        }
      }


    return(
        <>
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new hat</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange = {handleFabricChange} placeholder="Fabric" required type="text" id="fabric" name="fabric" value = {fabric} className="form-control" />
                <label htmlFor="fabric">Fabric</label>
              </div>
              <div  className="form-floating mb-3">
                <input onChange = {handleStyleNameChange} placeholder="Style Name" required type="text" id="style_name" name="style_name" value = {styleName} className="form-control" />
                <label htmlFor="style_name">Style Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange = {handleColorChange} placeholder="Color" required type="text" id="color" name="color" value = {color} className="form-control" />
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange = {handlePictureUrlChange} placeholder="Picture URL" required type="text" id="picture_url" name="picture_url" value = {pictureUrl} className="form-control" />
                <label htmlFor="picture_url">Picture URL</label>
              </div>
              <div className="mb-3">
                <select onChange = {handleLocationChange} required id="location" name="location" value = {location} className="form-select">
                  <option value="">Choose location</option>
                    {locations.map(location => {
                        return (
                            <option key = {location.href} value={location.href}>
                                {location.closet_name}
                            </option>
                        );
                    })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
      </>
    );
}

export default HatForm;
