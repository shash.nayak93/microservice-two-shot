from common.json import ModelEncoder
from .models import Shoes, BinVO
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json


class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "closet_name",
        "import_href",
    ]


# Create your views here.
class ShoesDetailEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "manufacturer",
        "modelName",
        "color",
        "picture_url",
        "bins",
    ]
    encoders = {
        "bins": BinVODetailEncoder(),
    }


class ShoesListEncoder(ModelEncoder):
    model = Shoes
    properties = ["modelName", "id"]
    #important to add id here

    def get_extra_data(self, o):
        return {"bins": o.bins.closet_name}


@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bin_vo_id=None):
    if request.method == "GET":
        if bin_vo_id is not None:
            shoes = Shoes.objects.filter(bins=bin_vo_id)
        else:
            shoes = Shoes.objects.all()
        return JsonResponse(
            {"shoes": shoes}, encoder=ShoesListEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            bin_href = content["bins"]
            bins = BinVO.objects.get(import_href=bin_href)
            content["bins"] = bins
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bins id"},
                status=400,
            )
        shoes = Shoes.objects.create(**content)
        return JsonResponse(
            shoes, encoder=ShoesDetailEncoder, safe=False
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_shoes(request, id):
    if request.method == "GET":
        shoes = Shoes.objects.get(id=id)
        return JsonResponse(
            shoes,
            encoder=ShoesDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Shoes.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
    try:
        if "bins" in content:
            bins = BinVO.objects.get(id=content["bins"])
            content["bins"] = bins
    except BinVO.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid bins id"},
            status=400,
        )
    Shoes.objects.filter(id=id).update(**content)
    attendee = Shoes.objects.get(id=id)
    return JsonResponse(
        attendee, encoder=ShoesDetailEncoder, safe=False
    )
